import firebase from "firebase/app";
import "firebase/auth";

export const auths = firebase
  .initializeApp({
    apiKey: "AIzaSyBW1PUWQtZhZmEx8DWGVJjAgwQhi3K1_Tw",
    authDomain: "auth-demo-c1925.firebaseapp.com",
    projectId: "auth-demo-c1925",
    storageBucket: "auth-demo-c1925.appspot.com",
    messagingSenderId: "746557330045",
    appId: "1:746557330045:web:059113e6cc70c0bb8d1714",
    // apiKey: "AIzaSyBPOptKg-sQ7jsVCI1nID1pCnGp4AI-qpo",
    // authDomain: "chatbox-b56b7.firebaseapp.com",
    // projectId: "chatbox-b56b7",
    // storageBucket: "chatbox-b56b7.appspot.com",
    // messagingSenderId: "501875142803",
    // appId: "1:501875142803:web:a7e1f1b6237337e6d29d66",
  })
  .auth();
