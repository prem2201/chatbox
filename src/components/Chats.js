import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { ChatEngine } from "react-chat-engine";
import { useHistory } from "react-router-dom";
import { useAuth } from "../context/AuthContext";
import { auths } from "../firebase";
export default function Chats() {
  const didMountRef = useRef(false);
  const [loading, setLoading] = useState(true);
  const { user } = useAuth();
  const history = useHistory();

  async function handleLogout() {
    await auths.signOut();
    history.push("/");
  }

  async function getFile(url) {
    let response = await fetch(url);
    let data = await response.blob();
    return new File([data], "test.jpg", { type: "image/jpeg" });
  }

  useEffect(() => {
    if (!user || user === null) {
      history.push("/");
      return;
    }
    console.log("ok");
    console.log(user.email);
    console.log(user.uid);

    axios
      .get("https://api.chatengine.io/users/me/", {
        headers: {
          "project-id": "f398dcdc-8499-4804-b2f2-49fcdbf7a5bd",
          "user-name": user.email,
          "user-secret": user.uid,
        },
      })

      .then(() => {
        console.log("ok");
        console.log(user.email);
        console.log(user.uid);
        setLoading(false);
      })

      .catch((e) => {
        let formdata = new FormData();
        formdata.append("email", user.email);
        formdata.append("username", user.email);
        formdata.append("secret", user.uid);
        getFile(user.photoURL).then((avatar) => {
          formdata.append("avatar", avatar, avatar.name);
        });
        axios
          .post("https://api.chatengine.io/users/", formdata, {
            headers: {
              "private-key": "29f365aa-d3e0-4323-bdc0-bb9754e84d23",
            },
          })
          .then(() => setLoading(false))
          .catch((e) => console.log("e", e.response));
      });
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  }, [user, history]);

  if (!user || loading) return "loading";

  return (
    <div className="chats-page">
      <div className="nav-bar">
        <div className="logo-tab">ChatBox</div>
        <div onClick={handleLogout} className="logout-tab">
          Logout
        </div>
      </div>
      <div style={{ backgroundColor: "red" }}>
        <ChatEngine
          height="calc(100vh - 66px)"
          projectID="f398dcdc-8499-4804-b2f2-49fcdbf7a5bd"
          userName={user.email}
          userSecret={user.uid}
        />
      </div>
    </div>
  );
}
