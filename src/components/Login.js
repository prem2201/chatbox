import { GoogleOutlined } from "@ant-design/icons";
import firebase from "firebase/app";
import React from "react";
import { auths } from "../firebase";

export default function Login() {
  return (
    <div id="login-page">
      <div id="login-card">
        <h2>Welcome to ChatBox</h2>

        <div
          className="login-button google"
          onClick={() =>
            auths.signInWithRedirect(new firebase.auth.GoogleAuthProvider())
          }
        >
          <GoogleOutlined /> Sign In with Google
        </div>

        <br />
      </div>
    </div>
  );
}
